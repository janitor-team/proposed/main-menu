# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Simplified Chinese translation for Debian Installer.
#
# Copyright (C) 2003-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Translated by Yijun Yuan (2004), Carlos Z.F. Liu (2004,2005,2006),
# Ming Hua (2005,2006,2007,2008), Xiyue Deng (2008), Kov Chai (2008),
# Kenlen Lai (2008), WCM (2008), Ren Xiaolei (2008).
#
#
# Translations from iso-codes:
#   Tobias Toedter <t.toedter@gmx.net>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
#
#   Free Software Foundation, Inc., 2002, 2003, 2007, 2008.
#   Alastair McKinstry <mckinstry@computer.org>, 2001,2002.
#   Translations taken from KDE:
#   - Wang Jian <lark@linux.net.cn>, 2000.
#   - Carlos Z.F. Liu <carlosliu@users.sourceforge.net>, 2004 - 2006.
#   LI Daobing <lidaobing@gmail.com>, 2007, 2008, 2009, 2010.
#   YunQiang Su <wzssyqa@gmail.com>, 2011.
#
#   Mai Hao Hui <mhh@126.com>, 2001 (translations from galeon)
# YunQiang Su <wzssyqa@gmail.com>, 2010, 2011, 2012, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2014-03-05 14:59+0800\n"
"Last-Translator: YunQiang Su <wzssyqa@gmail.com>\n"
"Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Type: text
#. Description
#. :sl1:
#: ../main-menu.templates:1001
msgid "Debian installer main menu"
msgstr "Debian 安装程序主菜单"

#. Type: select
#. Description
#. :sl1:
#: ../main-menu.templates:2001
msgid "Choose the next step in the install process:"
msgstr "请选择下一个安装步骤："

#. Type: error
#. Description
#. :sl2:
#: ../main-menu.templates:3001
msgid "Installation step failed"
msgstr "安装步骤失败"

#. Type: error
#. Description
#. :sl2:
#: ../main-menu.templates:3001
msgid ""
"An installation step failed. You can try to run the failing item again from "
"the menu, or skip it and choose something else. The failing step is: ${ITEM}"
msgstr ""
"执行某个安装步骤失败。您可以尝试从菜单中重新运行这个失败的项目，或跳过它并选"
"择其他项目。失败的步骤是：${ITEM}"

#. Type: select
#. Description
#. :sl2:
#: ../main-menu.templates:4001
msgid "Choose an installation step:"
msgstr "请选择一个安装步骤："

#. Type: select
#. Description
#. :sl2:
#: ../main-menu.templates:4001
msgid ""
"This installation step depends on one or more other steps that have not yet "
"been performed."
msgstr "此安装步骤依赖于另外的一个或多个尚未进行的步骤。"
